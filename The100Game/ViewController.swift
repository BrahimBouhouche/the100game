//
//  ViewController.swift
//  The100Game
//
//  Created by mac on 10/17/18.
//  Copyright © 2018 Brahim Bouhouche. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // variables for the game
    var currentValue: Int = 0
    var targetValue: Int = 0
    var score: Int = 0
    var round: Int = 1
    
    @IBOutlet weak var targetLabel: UILabel!
    @IBOutlet weak var slider: UISlider!
    
    @IBOutlet weak var scoreLabel: UILabel!
    
    @IBOutlet weak var roundLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startTheGame()
    }
    // the player slide the bull's eye
    @IBAction func sliderMoved(_ slider : UISlider){
        currentValue = lroundf(slider.value*100)
        print("the value is :\(currentValue)")
        
    }
    
    //when the player press hit me to check his slided value
    @IBAction func showAlert(_ sender: Any) {
        let diff = targetValue - lroundf(slider.value*100)
        
        if diff > 0 {
            let meassge = "The value of the slider :\(currentValue)"
            let alert = UIAlertController(title: "High value", message: meassge, preferredStyle: .alert)
            
            let action = UIAlertAction(title: "Again", style: .default) { (UIAlertAction) in
                self.round = self.round + 1
                self.score = self.score + self.currentValue
                self.startTheGame()
            }
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
           
        }else if diff == 0{
            let meassge = "The value of the slider :\(currentValue)"
            
            let alert = UIAlertController(title: "Jackpot !!!!!", message: meassge, preferredStyle: .alert)
            
            let action = UIAlertAction(title: "Again", style: .default) { (UIAlertAction) in
                self.round = self.round + 1
                self.score = self.score + 500
                self.startTheGame()
            }
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        
        }else{
            let meassge = "The value of the slider :\(currentValue)"
            let alert = UIAlertController(title: "Low value", message: meassge, preferredStyle: .alert)
            let action = UIAlertAction(title: "Again", style: .default) { (UIAlertAction) in
                self.round = self.round + 1
                self.score = self.score + self.currentValue
                self.startTheGame()
            }
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
          
        }
        
       
    }
    
    //initial all the labels with the right data

    func startTheGame(){
        
        targetValue = Int(arc4random_uniform(100))
        currentValue = lroundf(slider.value*100)
        print(currentValue)
        roundLabel.text = String(round)
        targetLabel.text = String(targetValue)
        scoreLabel.text = String(score)
        print(targetValue)
        
    }
    
    @IBAction func startOver(_ sender: Any) {
        score = 0
        round = 1
        slider.setValue(0.5, animated: true)
        startTheGame()
    }
    
    
    
    
}

